import sys
import time
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import os, platform, time
from datetime import datetime, date
from random import shuffle

cred = credentials.Certificate('empodelas-firebase-adminsdk-jssaw-a2231ef64a.json')
firebase_admin.initialize_app(cred)

db = firestore.client()
families = db.collection('Family').get()

DIAS = [
    'segunda',
    'terca',
    'quarta',
    'quinta',
    'sexta',
    'sabado',
    'domingo'
]
today = date.today()
indice_da_semana = today.weekday()
dia_da_semana = DIAS[indice_da_semana]

def arrumar_casa(age):
    if (age > 14): return 'Arrumar a casa toda'
    if (age > 12): return 'Limpar banheiro' or 'Passar pano no chao'
    if (age > 9): return 'Limpar móveis'
    if (age > 6): return 'Varrer' or 'Passar aspirador de pó'
    if (age > 4): return 'Tirar o pó'
    if (age > 2): return 'Guardar os brinquedos' or 'guardar os sapatos'
    return False

def fazer_lanche(age):
    if(age > 9): return 'Preparar lanche'
    if(age > 2): return 'Pegar frutas e legumes'
    return False

def cozinhar(age):
    if(age > 14): return 'Fazer a refeição'
    if(age > 9): return 'Ajudar a fazer a refeição'
    if(age > 6): return 'Lavar louça' or 'Por e tirar a mesa'
    if(age > 4): return 'Preparar e tirar a mesa'
    return False

def lavar_roupa(age):
    if(age > 12): return 'Por roupa para lavar'
    if(age > 6): return 'Pendurar roupas no varal'
    if(age > 4): return 'Colocar roupa na máquina' or 'Guardar roupa limpa'
    return False

def tirar_lixo(age):
    if(age > 6): return 'Tirar o lixo'
    if(age > 4): return 'Separar o lixo'
    return False

def arrumar_cama(age):
    if(age > 4): return 'Arrumar cama'
    return False

def trocar_roupa_cama(age):
    if(age > 9): return 'Trocar roupa de cama'
    return False

def regar_plantas(age):
    if(age > 4): return 'Regar plantas'
    return False

def fazer_compras(age):
    if(age > 14): return 'Fazer compras'
    if(age > 12): return 'Ajudar nas compras'
    if(age > 9): return 'Fazer lista de compras'
    if(age > 6): return 'Guardar as compras'
    return False

def lavar_carro(age):
    if(age > 14): return 'Lavar carro'
    return False

def dar_banho_animal(age):
    if(age > 12): return 'Dar banho no animal'
    return False

def dar_comida_animal(age):
    if(age > 9): return 'Dar comida ao animal'
    return False

def cuidar_parente(age):
    if(age > 12): return 'Cuidar de um parente'
    return False

def verify_list(age, item):
    switch = {
        'Arrumar a casa': arrumar_casa(age),
        'Fazer lanche': fazer_lanche(age),
        'Cozinhar': cozinhar(age),
        'Lavar roupa': lavar_roupa(age),
        'Tirar o lixo': tirar_lixo(age),
        'Arrumar a cama': arrumar_cama(age),
        'Trocar a roupa de cama': trocar_roupa_cama(age),
        'Regar as plantas': regar_plantas(age),
        'Fazer compras': fazer_compras(age),
        'Lavar o carro': lavar_carro(age),
        'Dar banho no animal': dar_banho_animal(age),
        'Dar comida para o animal': dar_comida_animal(age),
        'Cuidar de um parente': cuidar_parente(age),
    }
    func = switch.get(item)
    return func

def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

def toall(item):
    switch = {
        'Arrumar a casa': True,
        'Fazer lanche': False,
        'Cozinhar': False,
        'Lavar roupa': False,
        'Tirar o lixo': False,
        'Arrumar a cama': True,
        'Trocar a roupa de cama': True,
        'Regar as plantas': False,
        'Fazer compras': False,
        'Lavar o carro': False,
        'Dar banho no animal': False,
        'Dar comida para o animal': False,
        'Cuidar de um parente': False,
    }
    return switch.get(item)

def principal(age, item):
    switch = {
        'Arrumar a casa': True if (age > 14) else False,
        'Fazer lanche': True if (age > 9) else False,
        'Cozinhar': True if (age > 14) else False,
        'Lavar roupa': True if (age > 12) else False,
        'Tirar o lixo': True if (age > 6) else False,
        'Arrumar a cama': True if (age > 4) else False,
        'Trocar a roupa de cama': True if (age > 9) else False,
        'Regar as plantas': True if (age > 4) else False,
        'Fazer compras': True if (age > 14) else False,
        'Lavar o carro': True if (age > 14) else False,
        'Dar banho no animal': True if (age > 12) else False,
        'Dar comida para o animal': True if (age > 9) else False,
        'Cuidar de um parente': True if (age > 12) else False,
    }
    return switch.get(item)

for family in families:
    users = db.collection('User').where('family', '==', family.id).get()
    tasks = family.to_dict()[dia_da_semana]
    total_user = len(users)
    shuffle(tasks)

    final = []
    for j in range(total_user):
        final.append([])
    i = 0
    userInicial = -1
    ajuda = False
    
    while (i < len(tasks)):
        todos = False
        for j in range(total_user):
            if(i >= len(tasks)):
                break
            
            item = tasks[i]
            todos = toall(item)

            birth = users[j].to_dict()['birth']
            birth_date = datetime.strptime(birth, "%d/%m/%Y")
            age = calculate_age(birth_date)
            
            acao = verify_list(age, item)
            
            if(acao != False):
                if(principal(age, item)):
                    if (ajuda):
                        item = item + ' com ajudante'
                        ajuda = False
                    if(todos):
                        if(userInicial == -1):
                            final[j].append(item)
                            userInicial = j
                        else:
                            if(userInicial == j):
                                i=i+1
                                userInicial = -1
                            else:
                                final[j].append(item)

                    else:
                        final[j].append(item)
                        i=i+1
                else:
                    ajuda = True
                    final[j].append(acao)
for index, user in enumerate(users):
    finalArray = []
    for i in final[index]:
        finalArray.append({'name': i, 'done': False})

    db.collection('User').document(user.id).update({'atividadesDia': finalArray})