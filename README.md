# UniLar

## Descrição

O projeto UniLar vai além de um simples gerenciador de tarefas convencional, ele busca integrar toda a família na realização dos afazeres domésticos. As tarefas a serem realizadas são selecionadas pelos responsáveis da família e distribuídas aos demais integrantes de maneira justa conforme a faixa etária.

Com o intuito de estimular a aprendizagem dos nosso usuários, incluímos atividades educativas relacionadas aos diferentes afazeres domésticos, além de utilizar técnicas de premiações e competitividade saudável entre famílias para incentivar a realização das tarefas distribuídas pelo sistema.

Os usuários tem a opção de fazer um cadastro para elaborar atividades e exercícios com temática de tarefas domésticas, podendo receber receita de acordo com o uso de outros usuários.

## Participantes

Luiza Aguiar Hansen\
Pamela Hansen Barros\
Larissa Barros Rapousa\
Jorrana Barros Rapousa

## Instalação
Para execução do sistema de gerenciamento é necessário ter instalado:

- npm
- vue/cli -> sudo npm install -g @vue/cli


## Executar o projeto

Para executar o sistema UniLar é necessário clonar o repositório, entrar na pasta   ''empodelasweb'' e executar os seguintes comandos:
```
yarn
yarn serve
```

Em seguida acesse http://localhost:8080/#/ pelo seu browser favorito (chrome/firefox/ou outro) e o sistema estará funcionando


## Gerenciador

O gerenciador de tarefas será executado todo dia 00h através do CI do gitlab, o código esta na pasta ''gerenciador''

