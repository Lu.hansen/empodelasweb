import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import store from '../store/index'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {

  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeResolve(async (to, from, next) => {
    const getters = store.getters
    const currentUser = getters['user/user'].loggedIn
    const hasFamily = getters['family/family'].hasFamily

    const notRequiresAuth = !to.meta.login
    const notRequiresFamily = !to.meta.family
    
    // se tiver logado
    if (currentUser) {
      // acessar uma pagina para deslogado
      if(notRequiresAuth) {
        // se tiver familia
        if (!hasFamily) next({ path: '/acesso' })
        // se não tiver familia
        else next({ path: '/inicial' })
      } else {
        // acessar uma página para quem n tem familia
        if (notRequiresFamily) {
          // se tiver familia
          if (hasFamily) next({ path: '/inicial' })
          else next()
        } else {
          // se não tiver familia
          if (!hasFamily) next({ path: '/acesso' })
          else next()
        }
      }
    } else {
      if (!notRequiresAuth) next({ path: '/' })
      else next()
    }
  })


  return Router
}
