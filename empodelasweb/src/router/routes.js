
const routes = [
  {
    path: '',
    component: () => import('../layouts/MyLayout.vue'),
    children: [
      { path: '/', component: () => import('../pages/Home.vue') },
      { path: '/login', component: () => import('../pages/Login.vue') },
      { path: '/cadastro', component: () => import('../pages/Cadastro.vue') },
      { path: '/acesso', component: () => import('../pages/PrimeiroAcesso.vue'), meta: { login: true }},
      { path: '/novaFamilia', component: () => import('../pages/NovaFamilia.vue'), meta: { login: true }},
      { path: '/inicial', component: () => import('../pages/Inicial.vue'), meta: { login: true, family: true }},
      { path: '/conectar', component: () => import('../pages/ConectarFamilias.vue'), meta: { login: true, family: true }}
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('../pages/Error404.vue')
  })
}

export default routes
