import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import family from './family'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

const store = new Vuex.Store({
  modules: {
    user,
    family
  }
})

export default store