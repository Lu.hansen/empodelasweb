import * as firebase from 'firebase'
import store from '../index'
import { Notify } from 'quasar'

export function SET_LOGGED_IN(state, value) {
  state.user.loggedIn = value
}

export function SET_USER(state, data) {
  state.user.data = data
}

export async function CADASTRO(state, data) {
  const db = firebase.firestore()

  return firebase
    .auth()
    .createUserWithEmailAndPassword(`${data.username}@empodelas.com`, data.senha)
    .then(async auth => {
      await db.collection("User")
        .doc(auth.user.uid)
        .set({ 
          name: data.nome,
          email: data.email,
          username: data.username,
          birth: data.dataNascimento,
          family: '',
          atividadesDia: ''
        })
        .then(() => {
          state.user.loggedIn = true
          this.$router.push('/acesso')
          Notify.create({
            type: 'positive',
            message: `Cadastro realizado com sucesso`
          })
        })
    })
    .catch(err => {
      if (err.code === 'auth/email-already-in-use') {
        Notify.create({
          type: 'negative',
          message: `Username já existente`
        })
      } else {
        console.log(err)
        Notify.create({
          type: 'negative',
          message: `Não foi possível realizar cadastro`
        })
      }
    })
}

export async function LOGIN(state, data) {
  const email = `${data.email}@empodelas.com`
  const password = data.password
  const db = firebase.firestore()

  await firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(async user => {
      state.user.loggedIn = true
      await db.collection('Family').where('responsaveis', 'array-contains', user.user.uid).limit(1).get().then(async snap => {
        if(!snap.empty) {
          store.dispatch('family/fetchFamily', { data: snap.docs[0].data(), id: snap.id })
        } else {
          await db.collection('Family').where('dependentes', 'array-contains', user.user.uid).limit(1).get().then(snap => {
            if(!snap.empty) {
              store.dispatch('family/fetchFamily', { data: snap.docs[0].data(), id: snap.id })
            }
          })
        }
      })
      this.$router.push('/inicial')
    }).catch(err => {
      console.log(err)
      if(err.code === 'auth/user-not-found') {
        Notify.create({
          type: 'negative',
          message: `Usuário não existe`
        })
      }
      if (err.code === 'auth/wrong-password') {
        Notify.create({
          type: 'negative',
          message: `Senha incorreta`
        })
      }
    })
}

export async function LOGOUT() {
  await firebase
    .auth()
    .signOut()
    .then(() => {
      store.dispatch('user/fetchUser', null)
      store.dispatch('family/fetchFamily', null)
      this.$router.push('/').catch(() => {})
    })
}
