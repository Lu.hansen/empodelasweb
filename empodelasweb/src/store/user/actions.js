export function fetchUser({ commit }, user) {
  commit("SET_LOGGED_IN", user !== null)
  if (user) {
    commit("SET_USER", {
      id: user.id,
      displayName: user.name,
      email: user.email,
      geralInformation: user
    })
  } else {
    commit("SET_USER", null)
  }
}
export async function login({ commit }, user) {
  await commit("LOGIN", user)
}
export async function cadastro({ commit }, user) {
  return commit("CADASTRO", user)
}

export async function logout({ commit }) {
  await commit("LOGOUT")
}