import * as firebase from 'firebase'
import { Notify } from 'quasar'
import store from '../index'

export function SET_HAS_FAMILY(state, value) {
  state.family.hasFamily = value
}

export function SET_FAMILY(state, data) {
  state.family.data = data
}

export async function CRIAR(state, data) {
  const db = firebase.firestore()
  var responsaveis = data.responsaveis.map(p => p.id)
  var dependentes = data.dependentes.map(p => p.id)
  var segunda = data.segunda
  var terca = data.terca
  var quarta = data.quarta
  var quinta = data.quinta
  var sexta = data.sexta
  var sabado = data.sabado
  var domingo = data.domingo

  await db.collection("Family")
    .add({ name: data.nome, responsaveis, dependentes, segunda, terca, quarta, quinta, sexta, sabado, domingo })
    .then(async snap => {
      var familyId = snap.id

      responsaveis.forEach(async r => {
        await db.collection("User").doc(r)
          .update({'family': familyId})
      })

      dependentes.forEach(async r => {
        await db.collection("User").doc(r)
          .update({'family': familyId})
      })
      await db.collection('Family').doc(snap.id).get().then(snap => {
        if(!snap.empty) {
          store.dispatch('family/fetchFamily', { data: snap.data(), id: snap.id })
        }
      })
      state.family.hasFamily = true
      // state.family.data = snap.data()
      this.$router.push('/inicial')
      Notify.create({
        type: 'positive',
        message: `Família criada com sucesso`
      })
    })
    .catch(err => {
      console.log(err)
      Notify.create({
        type: 'negative',
        message: `Não foi possível criar família`
      })
    })
}

export async function CONECTAR(state, data) {
  const db = firebase.firestore()
  if (data != state.family.data.id) {
    var otherFamily = await ENCONTRAR(state, data)

    var newData = state.family.data.data.conections || []
    newData.push(data)

    var newConectionsOtherFamily = otherFamily.conections || []
    newConectionsOtherFamily.push(state.family.data.id)

    db.collection("Family").doc(state.family.data.id).update({ conections: newData })
    db.collection("Family").doc(data).update({ conections: newConectionsOtherFamily })
    state.family.data.data.conections = newData
    Notify.create({
      type: 'positive',
      message: `Famílias conectadas`
    })
  } else {
    Notify.create({
      type: 'negative',
      message: `Esse código é da sua família, tente outro código`
    })
  }
}

export async function ENTRAR(state, data) {
  const db = firebase.firestore()
  var newFamily = await ENCONTRAR(state, data)
  var user = store.getters['user/user']
  await db.collection("User").doc(user.data.id).update({'family': data})

  newFamily.dependentes.push(user.data.id)
  await db.collection("Family").doc(data).update({'dependentes': newFamily.dependentes})

  state.family.hasFamily = true
  state.family.data = newFamily
  Notify.create({
    type: 'positive',
    message: `Você entrou na família com sucesso`
  })
  this.$router.push('/inicial')
}

export async function ENCONTRAR(state, data) {
  const db = firebase.firestore()
  return db.collection('Family').doc(data).get()
    .then(snap => {
      if (!snap.exists) {
        Notify.create({
          type: 'negative',
          message: `Essa família não existe`
        })
        throw new Error
      } else {
        return snap.data()
      }
    })
}
