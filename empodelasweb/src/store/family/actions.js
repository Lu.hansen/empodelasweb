export async function criar({ commit }, data) {
  await commit("CRIAR", data)
}

export async function entrar({ commit }, data) {
  await commit("ENTRAR", data)
}

export async function conectar({ commit }, data) {
  await commit("CONECTAR", data)
}

export function fetchFamily({ commit }, data) {
  commit("SET_HAS_FAMILY", data !== null)
  if (data) {
    commit("SET_FAMILY", {
      id: data.id,
      data: data
    })
  } else {
    commit("SET_FAMILY", null)
  }
}