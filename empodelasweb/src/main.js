import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import './quasar'
import * as VeeValidate from 'vee-validate'

import createStore from './store/index.js'

import createRouter from './router/index.js'

import * as firebase from 'firebase'

Vue.config.productionTip = false

// create store and router instances
Vue.use(VeeValidate)

const store = typeof createStore === 'function'
  ? createStore()
  : createStore

const router = typeof createRouter === 'function'
  ? createRouter({store})
  : createRouter

// make router instance available in store
store.$router = router

const firebaseConfig = {
  apiKey: "AIzaSyBwvyftg87ursxievzEo6nRk050CkDcklQ",
  authDomain: "empodelas.firebaseapp.com",
  databaseURL: "https://empodelas.firebaseio.com",
  projectId: "empodelas",
  storageBucket: "empodelas.appspot.com",
  messagingSenderId: "1073078410192",
  appId: "1:1073078410192:web:5f439a5d47fbd132e1f7d5",
  measurementId: "G-X0L897SB3Z"
};
firebase.initializeApp(firebaseConfig)
var app = ''

var db = firebase.firestore()

// Initialize Firebase
firebase.auth().onAuthStateChanged(async user => {
  var userDetails = null
  var familyId = null
  if (user) {
    await db.collection('User').doc(user.uid).get().then(async snap => {
      userDetails = snap.data()
      userDetails['id'] = snap.id
      familyId = userDetails.family
      if (familyId) {
        await db.collection('Family').doc(familyId).get().then(snap => {
          store.dispatch('family/fetchFamily', { data: snap.data(), id: snap.id })
        })
      }
    })
  }
  store.dispatch('user/fetchUser', userDetails)


  if (!app) {
    new Vue({
      vuetify,
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})

// Vue.prototype.$firebase = firebase;
// Vue.config.productionTip = false;
